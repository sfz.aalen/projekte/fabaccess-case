# Fabaccess Case
Dieses Projekt bietet ein alternatives 3D-Druck Gehäuse für den [Fabreader](https://gitlab.com/fabinfra/fabhardware/fabreader/-/tree/master/).

## Bilder
![The Casing, fully assembled](./media/full.jpeg)
![The Interior Electronics](./media/cabeling.jpeg)
![The Back](./media/back.jpeg)
Mit Lasercutter kann man einen Schritt weitergehen.
![Lasercutter Logo](./media/with_logo.jpeg)

## Assembly
Zusätzlich zu den vom Fabreader benötigten Komponenten werden (optional zur stromversorgung) benötigt:
- USB-C Breakout Board for TP4056 (siehe obere bilder für benötigte form)
- Beliebiger Kondensator mit > 5V zur Überbrückung der Batterie-Kontakte des Ladereglers damit dieser eine Batterie erkennt und die 5V vom USB-C an das Mainboard durchreicht.

Die .stl dateien können im artifacts Ordner gefunden werden.
Es wird empfohlen die beiden teile folgendermaßen zu slicen/drucken (hier gezeigt an prusaslicer, grün ist support):
![Cover](./media/slice_front.png)
![Back](./media/slice_back.png)

Danach kann das Case folgendermaßen zusammengesteckt werden:
![Assembly](./media/assembly.gif)

Dabei sollte drauf geachtet werden dass der Kondensator und die Stromkabel angebracht werden bevor das USB-C Board in das Case eingebaut werden.

WICHTIG: Das USB-C Board kann nach dem feststecken nicht wieder entfernt werden ohne das Case zu beschädigen.

Es wird empfohlen die Stromkabel länger als notwendig zu machen, um beim rausnehmen der zentralen Platinen die Chance to verringern die Kabel abzureißen.

## Addons
Im moment stehen folgede Addons zur verfügung:
- Card Holder

Diese können gefunden werden im entsprechenden Artifacts-Verzeichnis. Teile die nicht dort gefunden werden sind unverändert zu der basis Version.
