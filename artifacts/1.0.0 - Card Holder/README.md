# Card Holder

Attachment um eine Karte an dem Case festzuhalten.
![Card holder with card](./media/with_card.jpeg)
![Card holder without card](./media/without_card.jpeg)

## Assembly

Das main Case kann identisch zu der basis Version gedruckt werden, das Attachment wird empfohlen folgendermaßen zu drucken:

![The Crab in its full glory](./media/holder_print.png)

Danach kann der Holder in die dafür vorgesehen Löcher gesteckt werden. Dabei sollte acht genommen werden dass der Holder dabei nicht zerbricht (sollte eigentlich nicht passieren).